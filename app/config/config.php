<?php
/**
 * @package Config
 * @author Pritam
 */

/** Environment 'production' or 'dev' **/
$env = "dev";
/** Set Default Controllers in app/controllers **/
$default_controller = "Welcome";