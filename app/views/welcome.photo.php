
<!doctype html>
<html>
<head>
    <title>{{ echo $example_title ; }}</title>
    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
</head>

<body>
<div>
    {{ print_r( $this->get_results($this,"select * from user_names;") ); }}
    <h1>{{ echo $example_title ; }}</h1>
    <p>{{ echo $desc; }}</p>
</div>
</body>
</html>
