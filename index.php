<?php
/**
 * @package Index
 * @author Pritam
 */
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/sys/Application.php';
require __DIR__ . '/app/config/config.php';
use Josantonius\ErrorHandler\ErrorHandler;
new ErrorHandler;
if(!file_exists(".cache")){
	mkdir(".cache");
}
$getClasses = array_diff(scandir("app/classes"), array('.', '..'));
foreach ($getClasses as $key => $value) {
	include("app/classes/".$value."");
}
if(isset($_GET['controller'])){
	if(file_exists(__DIR__ ."/app/controllers/".$_GET['controller'].".php")){
		include __DIR__ ."/app/controllers/".$_GET['controller'].".php";
		$view = new $_GET['controller']();
	}else{
		$message = "404 not found";
		$page = $_GET['controller'];
		$requestedView = $app->ReqViews("not_found");
		include(".cache/".$requestedView."");
		unlink(".cache/".$requestedView."");
	}
}else{
	include __DIR__ ."/app/controllers/".$default_controller.".php";
	$view = new $default_controller();
}