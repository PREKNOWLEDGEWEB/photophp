<?php
/**
 * @author Pritam
 * @package Application
 */
class App
{
	public $enableAdvancedPHP = true;
	public $mysqlHost = 'localhost';
	public $mysqlUsername = 'root';
	public $mysqlPassword = '';
	public $mysqlDatabase = 'test_fullchat';
	function __construct()
	{
		
	}
	function setenableAdvancedPHP($val){
		$this->enableAdvancedPHP = $val;
	}
	function ReqViews($views_name){
		if($this->ifViewExists($views_name)){
			$file = $this->get_views_file($views_name);
			$publichtml = file_get_contents("public/index.html");
			$publichtml = str_replace("<[body]>",$file,$publichtml);
			$cp = $this->replace_collumswith($publichtml);
			$enc = md5(uniqid(rand(2,22), true));
			$createCache = fopen(".cache/temp_".$enc.".php", "w") or throw new Exception("Oops unable to create cache", 1);
			fwrite($createCache, $cp);
			fclose($createCache);
			return "temp_".$enc.".php";
		}else{
			throw new Exception("Error Views not found", 1);
		}
	}
	function clearCache(){
		$path = ".cache";
		if($this->rrmdir($path)) {
		  return true;
		}else{
			return false;
		}
	}
	public function assets(){
		echo("
			<style></style>
		");
	}
	public function connectMysqli(){
		return mysqli_connect($this->mysqlHost,$this->mysqlUsername,$this->mysqlPassword,$this->mysqlDatabase);
	}
	public function query_execute($context,$query){
		$prepareQuery = mysqli_query($context->connectMysqli(),$query);
		return $prepareQuery;
	}
	public function get_results($context,$query){
		$prepareQuery = mysqli_query($context->connectMysqli(),$query);
		if (mysqli_num_rows($prepareQuery) > 0) {
		$i=0;
		$data['data'] = array();
		$data['count'] = 0;
		while($row = mysqli_fetch_array($prepareQuery)) {
			$data['count'] = $data['count'] + 1;
			$data['data'][] = array($row);
		$i++;
		} return $data; }
		else{
		    return array('count'=>0,'data'=>array());
		}
	}
	public function rrmdir($dir) { 
	   if (is_dir($dir)) { 
	     $objects = scandir($dir);
	     foreach ($objects as $object) { 
	       if ($object != "." && $object != "..") { 
	         if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
	           rrmdir($dir. DIRECTORY_SEPARATOR .$object);
	         else
	           unlink($dir. DIRECTORY_SEPARATOR .$object); 
	       } 
	     }
	     rmdir($dir); 
	   } 
	}
	public function ifViewExists($file){
		if(file_exists("../app/views/".$file.".photo.php")){
			return true;
		}elseif(file_exists("app/views/".$file.".photo.php")){
			return true;
		}else{
			return false;
		}
	}
	public function get_views_file($file){
		if(file_exists("../app/views/".$file.".photo.php")){
			return file_get_contents("../app/views/".$file.".photo.php");
		}elseif(file_exists("app/views/".$file.".photo.php")){
			return file_get_contents("app/views/".$file.".photo.php");
		}else{
			return false;
		}
	}
	public function replace_collumswith($text){
		$text = str_replace("{{","<?php",$text);
		$text = str_replace("}}","?>",$text);
		return $text;
	}
}
$app = new App();